#!/usr/bin/python3

# Script to get the allow-stderr from salsa.debian.org

import argparse
import requests
import json


def get_package_list(projectid):
    # Get the package id from the salsa.debian.org API
    url = f"https://salsa.debian.org/api/v4/groups/{projectid}/projects?per_page=100"

    more_pages = True
    packages = []
    while more_pages:
        if (args.debug):
            print(f"URL: {url}")
        r = requests.get(url)
        if (r.status_code == 200):
            # Get the list of packages
            packages_got = r.json()
            packages.extend(packages_got)
            if (args.debug):
                print(f"Number of packages: {len(packages_got)}")
                print("Get next page")
            # get the next page if exists
            try:
                next_page = r.links['next']['url']
                url = next_page
                if (args.debug):
                    print(f"Next page: {next_page}")
            except KeyError:
                more_pages = False

    return packages


def get_url(url):
    if (args.debug):
        print(f"URL: {url}")
    r = requests.get(url)
    if (r.status_code != 200):
        if (args.debug):
            print(f"Error: {r.status_code}")
            print(f"URL: {url}")
            print(f"Response: {r.text}")
        return False
    return True


def trigger_pipeline(package, token):
    print(f"Trigger pipeline for {package}")
    url = f"https://salsa.debian.org/api/v4/projects/horde-team%2F{package}/trigger/pipeline?token={token}&ref=debian-sid"
    if (args.debug):
        print(f"URL: {url}")
    r = requests.post(url)
    if (r.status_code != 201):
        print(f"Error: {r.status_code}")
        print(f"URL: {url}")
        print(f"Response: {r.text}")
        return False
    return True


if __name__ == "__main__":
    # Read the command line arguments
    parser = argparse.ArgumentParser(
        description='Get the allow-stderr from salsa.debian.org')
    parser.add_argument(
        '-p', '--package', help='The package to check. By Default - all packages from ghe Debian Horde Team group (id 3290)')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug output')

    args = parser.parse_args()
    packages = get_package_list(3290)

    print(f"Total number of packages: {len(packages)}")

    packages_no_debian_sid = []
    packages_no_debian_tests_control = []
    packages_no_debian_salsa_ci_yml = []
    packages_with_salsa_ci_yml = []
    packages_no_allow_stderr = []
    packages_with_allow_stderr = []
    # Get the allow-stderr from the package
    for i, package in enumerate(packages):
        print(f"{i+1}/{len(packages)}: {package['path']}")
        # Check, whether debian-sid branch in the repository exists
        if (not (get_url(f"https://salsa.debian.org/api/v4/projects/horde-team%2F{package['path']}/repository/branches/debian-sid"))):
            packages_no_debian_sid.append(package['path'])
            print(f"{package['path']} does not have debian-sid branch")
            continue

        # check, whether the file debian/salsa-ci.yml exists in the branch debian-sid
        url = f"https://salsa.debian.org/api/v4/projects/horde-team%2F{package['path']}/repository/files/debian%2Fsalsa-ci.yml/raw?ref=debian-sid"
        if (args.debug):
            print(f"URL: {url}")
        r = requests.get(url)
        if (r.status_code != 200):
            if (args.debug):
                print(f"Error: {r.status_code}")
                print(f"URL: {url}")
                print(f"Response: {r.text}")
            packages_no_debian_salsa_ci_yml.append(package['path'])
            print(
                f"{package['path']} does not have debian/debian/salsa-ci.yml file")
            continue
        else:

            outStr = package['path']
            # Get the state of the pipeline
            url = f"https://salsa.debian.org/api/v4/projects/horde-team%2F{package['path']}/pipelines?ref=debian-sid"
            if (args.debug):
                print(f"URL: {url}")
            r = requests.get(url)
            if (r.status_code != 200):
                print(f"Error: {r.status_code}")
                outStr += f"; Error in pipeline status: {r.status_code}"
            else:
                # Get the state of the pipeline
                pipelines = r.json()
                if (len(pipelines) == 0):
                    print("No pipelines found")
                    outStr += "; No pipelines found"
                else:
                    if (pipelines[0]['status'] == 'success'):
                        print("Pipeline is successful")
                        outStr += f"; Pipeline is successful on {pipelines[0]['updated_at']}"
                    else:
                        print("Pipeline is not successful")
                        outStr += f"; Pipeline is not successful on {pipelines[0]['updated_at']}"
            packages_with_salsa_ci_yml.append(outStr)

        # check, whether the file debian/tests/control exists in the branch debian-sid
        url = f"https://salsa.debian.org/api/v4/projects/horde-team%2F{package['path']}/repository/files/debian%2Ftests%2Fcontrol/raw?ref=debian-sid"
        if (args.debug):
            print(f"URL: {url}")
        r = requests.get(url)
        if (r.status_code != 200):
            if (args.debug):
                print(f"Error: {r.status_code}")
                print(f"URL: {url}")
                print(f"Response: {r.text}")
            packages_no_debian_tests_control.append(package['path'])
            print(f"{package['path']} does not have debian/tests/control file")
            continue

        # Get the content of the file debian/tests/control and check, whether it contains allow-stderr
        filecontent = r.text
        if (args.debug):
            print(f"File content: {filecontent}")

        if (filecontent.find("allow-stderr") != -1):
            if args.debug:
                print(f"{package['path']} has allow-stderr")
            packages_with_allow_stderr.append(package['path'])
            print(f"{package['path']} has allow-stderr")

        else:
            if args.debug:
                print(f"{package['path']} does not have allow-stderr")
            packages_no_allow_stderr.append(package['path'])
            print(f"{package['path']} does not have allow-stderr")

    # Print the grouped summary
    print("Summary:")

    print(
        f"Number of packages without debian-sid branch: {len(packages_no_debian_sid)}")
    for package in packages_no_debian_sid:
        print(f"{package}")

    print(
        f"Number of packages without debian/tests/control file: {len(packages_no_debian_tests_control)}")
    for package in packages_no_debian_tests_control:
        print(f"{package}")

    print(
        f"Number of packages without debian/salsa-ci.yml file: {len(packages_no_debian_salsa_ci_yml)}")
    for package in packages_no_debian_salsa_ci_yml:
        print(f"{package}")

    print (
        f"Number of packages with debian/salsa-ci.yml file: {len(packages_with_salsa_ci_yml)}")
    for package in packages_with_salsa_ci_yml:
        print (f"{package}")

    print(
        f"Number of packages without allow-stderr: {len(packages_no_allow_stderr)}")
    for package in packages_no_allow_stderr:
        print(f"{package}")

    print(
        f"Number of packages with allow-stderr: {len(packages_with_allow_stderr)}")
    for package in packages_with_allow_stderr:
        print(f"{package}")
