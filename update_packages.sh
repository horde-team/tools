#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
    echo_color "$Green$debian_name ($pear_name)$Color_Off"
    if [ ! -d "${git_dir}/.git" ]; then
        echo_color "- ${Yellow}Git dir not present. Skipping to next package${Color_Off}"
        continue
    fi
    cd $git_dir
    # Go to debian-sid branch
	debian_branch=$(get_branch 'debian')
    git checkout --quiet "${debian_branch}"
    orig_upstream_version="$(pkgtools phppear version)"
    # init vars
    upstream_version="0.0.0~howdidyougetthere"
    status=""
    # Get uscan info
    old_IFS=$IFS
    IFS=$'\n'
    for line in $(uscan --dehs --report 2>/dev/null)
    do
        key=${line/</}
        key=${key/>*/}
        value=${line/<\/*/}
        value=${value/*>/}
        if [ $key = "upstream-version" ]; then
            upstream_version="$value"
        elif [ $key = "status" ]; then
            status="$value"
        fi
    done
    if [ "$upstream_version" = "0.0.0~howdidyougetthere" ]; then
        echo_color "- ${Red}Problem running uscan, exiting.${Color_Off}"
        exit 1
    fi
    IFS=$old_IFS
    #
    if [ "$status" = "Newer version available" -o "$status" = "newer package available" -o "$status" = "" ]; then
        orig_upstream_version_major=$(echo $orig_upstream_version | cut -f 1 -d.)
        upstream_version_major=$(echo $upstream_version | cut -f 1 -d.)
        #download new tarball and update git branches
        echo_color "- ${Purple}New version available ${orig_upstream_version} -> ${upstream_version}$Color_Off"
        if [ "${auto_import}" = "yes" -a "${orig_upstream_version_major}" = "${upstream_version_major}" ]; then
            confirm_import=yes
            echo_color "- ${Green}Auto: import${Color_Off}"
        else
            read -p 'Do you want to import (type: "yes")? ' confirm_import
        fi
        if [ "${confirm_import}" != "yes" ]; then
            echo_color "- ${Yellow}Skipping to next package${Color_Off}"
            continue
        fi
        # Guess if upstream is repacked:
        git_import_orig_args=''
        if grep -qi 'filter-pristine-tar\s\+=\s\+true' debian/gbp.conf; then
            upstream_version="${upstream_version}+debian0"
            git_import_orig_args="--upstream-version=${upstream_version}"
        fi
        if [ -e "debian/patches/series" ]; then
            QUILT_PATCHES=debian/patches/ quilt pop -a || true
            rm .pc/ -Rf
        fi
        gbp import-orig --uscan --no-interactive $git_import_orig_args
        if [ -e "debian/patches/series" ]; then
            pkg_upstream_version=$(dpkg-parsechangelog -S Version | cut -d "-" -f1)
            sed -i debian/patches/*.patch -e "s/$pkg_upstream_version/$upstream_version/g"
            git commit debian/patches/*.patch -m "d/patches: Adapt path names in patch files to new upstream version."
        fi
        dch --release-heuristic changelog -v "$upstream_version-1" "New upstream version $upstream_version"
        git add debian/changelog
        git commit -m"New upstream version $upstream_version"
        git push --all
        git push --tags
    fi
done 7< "$pear_name_file"
