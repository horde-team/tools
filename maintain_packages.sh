#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

CURRENT_DH_COMPAT=13

# supported actions
#
# Add uploader: <script> add-uploader "Full Name <mail@domain.tld>"
# Drop uploader: <script> drop-uploader "Full Name <mail@domain.tld>"

if [ -n "$2" ]; then
	shift
	action="$1"

	if [ -n "$2" ]; then
		shift
		# needed for action/value commands
		value="$1"

		if [ "x$action" = "xgit" ] && [ -n "$2" ]; then

			# needed for git commands
			shift
			git_options="$@"

		elif [ "x$action" = "xquilt" ] && [ -n "$2" ]; then

			# needed for quilt commands
			shift
			quilt_options="$@"

		fi

	fi
fi

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
	pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
	pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
	if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
	fi
	# Go to some trusted directory (without debian/pkg-php-tools-overrides file)
	cd "$pkg_horde_dir/tools"
	debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
	if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping${Color_Off}"
		continue
	fi
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
	echo_color "$Green============------------$Color_Off"
	if [ ! -d "${git_dir}/.git" ]; then
		echo_color "- ${Yellow}Git dir not present. Skipping to next package${Color_Off}"
		continue
	fi
	cd $git_dir
	debian_branch=$(get_branch 'debian')
	git checkout "${debian_branch}"
	ret=0

	case "$action" in

		"add-uploader")
			uploader_fn="$(echo "${value}" | sed -r -e "s/(.*)<[^>]+>\s*/\1/" | sed -e 's/[[:space:]]*$//')"
			uploader_mail="$(echo "${value}" | sed -r -e "s/.*<([^>]+)>\s*/\1/")"
			uploaders_field=`grep -Pzo "Uploaders:.*\n( .*\n)+" debian/control | tr -d '\0'`
			if ! echo "$uploaders_field" | grep -q "$uploader_mail"; then
				sed -r -e "s/^(Uploaders:.*)>$/\1>,/" -i debian/control
				sed -r -e "/^Uploaders:.*/a \ $uploader_fn <$uploader_mail>," -i debian/control

				git commit debian/control -m "d/control: Add to Uploaders: $uploader_fn."
				PAGER=cat git show

			else
				echo_color "${Yellow}already an uploader: $uploader_fn <$uploader_mail>${Color_Off}"
			fi
			echo
			;;

		"drop-uploader")
			uploader_fn="$(echo "${value}" | sed -r -e "s/(.*)<[^>]+>\s*/\1/" | sed -e 's/[[:space:]]*$//')"
			uploader_mail="$(echo "${value}" | sed -r -e "s/.*<([^>]+)>\s*/\1/")"
			uploaders_field=`grep -Pzo "Uploaders:.*\n( .*\n)*" debian/control | tr -d '\0'`
			if echo "$uploaders_field" | grep -q "$uploader_mail"; then
				# appears in first line
				sed -r -e "s/^Uploaders:\s+$uploader_fn\ <$uploader_mail>.*/Uploaders:/" -i debian/control
				# appears below first line
				sed -r -e "/^ $uploader_fn\ <$uploader_mail>/d" -i debian/control

				git commit debian/control -m "d/control: Drop from Uploaders: $uploader_fn."
				PAGER=cat git show

			else
				echo_color "${Red}not an uploader: $uploader_fn <$uploader_mail>${Color_Off}"
			fi
			echo
			;;

		"drop-needs-recommends")
			if [ -e debian/tests/control ]; then
				if grep -q -E '^Restrictions: needs-recommends$' < debian/tests/control; then
					sed -re '/^Restrictions: needs-recommends$/d' -i debian/tests/control
					git commit debian/tests/control -m "d/tests/control: Stop using deprecated needs-recommends restriction."
					PAGER=cat git show
				else
					echo_color "${Yellow}obsolete needs-recommends restriction already removed from debian/tests/control${Color_Off}"
				fi
			else
					echo_color "${Red}no autopkgtests setup for this src:pkg${Color_Off}"
			fi
			echo
			;;

		"run-autopkgtest")
			if [ -e debian/tests/control ]; then
				debversion="$(dpkg-parsechangelog -S Version)"
				gbp buildpackage --no-sign --git-no-pbuilder -S -d
				if grep -qE "^Restrictions:.*isolation-container.*" < debian/tests/control; then
					autopkgtest --debug --shell -U "../build-area/${debian_name}_${debversion}.dsc" -- lxc -s -d autopkgtest-sid-amd64
				else
					autopkgtest --debug --shell -U "../build-area/${debian_name}_${debversion}.dsc" -- schroot -d sid-amd64-autopkgtest
				fi
			fi
			;;

		"bump-standards-version")
			version=$(echo ${value} | sed -e 's/[[:space:]]*$//')
			sed -r -e "s/^Standards-Version:.*/Standards-Version: $version/" -i debian/control
			if [ -n "$(git diff debian/control)" ]; then
				git commit debian/control -m "d/control: Bump Standards-Version: to $version. No changes needed."
				PAGER=cat git show
			else
				echo_color "${Yellow}already at Standards-Version: $version${Color_Off}"
			fi
			echo
			;;

		"bump-watch-format-version")
			version=$(echo ${value} | sed -e 's/[[:space:]]*$//')
			sed -r -e "s/^version=.*/version=$version/" -i debian/watch
			if [ -n "$(git diff debian/watch)" ]; then
				git commit debian/watch -m "d/watch: Switch to format version $version."
				PAGER=cat git show
			else
				echo_color "${Yellow}watch file already at format version: $version${Color_Off}"
			fi
			echo
			;;

		"bump-dh-compat-level")
			version=$(echo ${value} | sed -e 's/[[:space:]]*$//')
			sed -r -e "s/(.*)debhelper-compat\s+\(=\s*[0-9]+\)(.*)/\1debhelper-compat (= ${CURRENT_DH_COMPAT})\2/" -i debian/control
			if [ -n "$(git diff debian/control)" ]; then
				git commit debian/control -m "d/control: Bump DH compat level to version ${CURRENT_DH_COMPAT}."
				PAGER=cat git show
			else
				echo_color "${Yellow}already at DH compat level version ${CURRENT_DH_COMPAT}${Color_Off}"
			fi
			echo
			;;

		"add-r3-no")
			if grep -q -E "^Rules-Requires-Root:.*" debian/control; then
				echo_color "${Yellow}already has Rules-Requires-Root: field${Color_Off}"
			else
				sed -r -e "s/^(Standards-Version:.*)/\1\nRules-Requires-Root: no/" -i debian/control
				git commit debian/control -m "d/control: Add Rules-Requires-Root: field and set it to 'no'."
				PAGER=cat git show
			fi
			echo
			;;

		"add-upstream-metadata")
			if [ ! -e debian/upstream/metadata ]; then
				repo_name=`echo ${pear_name} | sed -r -e "s/Horde_(.*)/\1/"`
				mkdir -p debian/upstream
				cat << EOF > debian/upstream/metadata
Bug-Database: https://bugs.horde.org/
Bug-Submit: https://bugs.horde.org/ticket/create.php
Donation: https://www.horde.org/support
Repository: https://github.com/horde/${repo_name}.git
Repository-Browse: https://github.com/horde/${repo_name}/
Security-Contact: https://www.horde.org/apps/horde/docs/SECURITY
EOF
				git add debian/upstream/metadata
				git commit debian/upstream/metadata -m "d/upstream/metadata: Add file. Comply with DEP-12."
				git show
			else
				echo_color "${Yellow}upstream metadata file already present${Color_Off}"
			fi
			echo
			;;

		"add-salsa-ci-yml")
			if [ ! -e debian/salsa-ci.yml ]; then
				cat << EOF > debian/salsa-ci.yml
---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml

variables:
  SALSA_CI_DISABLE_APTLY: 0
EOF
				git add debian/salsa-ci.yml
				git commit debian/salsa-ci.yml -m "d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls."
				git show
			else
				echo_color "${Yellow}salsa-ci.yml already present${Color_Off}"
			fi
			echo
			;;

		"fix-php8-tests")

			cat /etc/debian_version | grep -q "/sid" || {
				echo_color "${Red}You must be in a Debian testing/sid chroot/system to fix PHPUnit tests. Aborting...${Color_Off}"
				echo
				exit 1
			}

			find */test -maxdepth 0 -type d 1>/dev/null 2>/dev/null || {
				echo_color "${Yellow}No PHPUnit tests found in ${pear_name}.${Color_Off}"
				echo
				continue
			}

			find Horde_*-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null || {

				# some exceptions
				if   find ansel-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find content-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find imp-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find ingo-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find kronolith-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find mnemo-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find nag-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find passwd-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find sesha-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find turba-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find wicked-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find horde_lz4-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				else
					echo_color "${Yellow}Not a Horde component, investigate manually, please.${Color_Off}"
					echo
					continue
				fi

			}

			all_tests_php_file=$(find */test/ -type f -name AllTests.php | head -n1)
			if [ -z "$all_tests_php_file" ]; then
				echo_color "${Red}Could not detect base folder of unit tests, skipping to next package.${Color_Off}"
				continue
			fi
			TEST_PATH=$(dirname $all_tests_php_file)

			export QUILT_PATCHES="$(pwd)/debian/patches"
			PATCH=1011_php-8.x.patch
			if [ -e "debian/patches/series" ]; then
				quilt push -a  1>/dev/null|| true
			fi
			cd ${TEST_PATH}
			if phpunit .; then
				echo_color "${Green}PHPUnit tests succeed, continuing with next package.${Color_Off}"
				cd - 1>/dev/null
				quilt pop -a -f 1>/dev/null || true
				echo
				continue
			elif ( [ -e debian/patches/series ] && ( [ -e "debian/patches/$PATCH" ] && [ -s "debian/patches/$PATCH" ] ) ); then
				echo_color "${Red}PHPUnit patch for ${Color_Off}$pear_name${Red} is already present, but tests fail!!!${Color_Off}"
				echo_color "${Yellow}Cannot auto-fix PHPUnit tests.\nPlease investigate manually...${Color_Off}"
				read -p "$(echo_color ${Yellow}Do you want to work on ${Red}${pear_name}${Yellow}\'s PHPUnit tests now? ${Color_Off})" -n 1 -r
				echo
				if ! [[ $REPLY =~ ^[Yy]$ ]]; then
					cd - 1>/dev/null
					continue
				fi
				echo
				echo_color "${Yellow}You have been chdir'ed to this location:${Color_Off}\n$(pwd)"
				echo
				echo_color "${Yellow}Run PHPUnit tests manually and possibly fix more things.${Color_Off}..."
				echo_color "${Yellow}Make sure to properly commit all your changes to Git before leaving.${Color_Off}"
				echo_color "${Yellow}Run tests with this cmdline: ${Color_Off}phpunit -v . | less"
				echo_color "${Yellow}Type '${Color_Off}exit 10${Yellow}' when done.${Color_Off}"

				bash || ret=$?
				if [ $ret = 10 ]; then
					echo_color "- ${Yellow}Continuing${Color_Off}"
					cd - 1>/dev/null
				else
					echo_color "- ${Yellow}Skipping to next package${Color_Off}"
					cd - 1>/dev/null
					continue
				fi

				cd - 1>/dev/null
				quilt pop -a -f 1>/dev/null || true
				echo
			else
				read -p "$(echo_color ${Yellow}Do you want to work on ${Red}${pear_name}${Yellow}\'s PHPUnit tests now? ${Color_Off})" -n 1 -r
				echo
				if ! [[ $REPLY =~ ^[Yy]$ ]]; then
					echo
					cd - 1>/dev/null
					continue
				fi
				cd - 1>/dev/null

				git reset --hard
				git clean -df

				if [ -e "$TEST_PATH/phpunit.xml" ]; then
					touch "$TEST_PATH/phpunit.xml"
				fi
				if [ ! -d debian/patches ]; then
					mkdir -p debian/patches
				fi

				export QUILT_PATCHES="$(pwd)/debian/patches"
				quilt push -a || true
				quilt new $PATCH
				quilt add $(find */test/ -type f)
				if [ ! -s "$TEST_PATH/phpunit.xml" ]; then
					quilt add "$TEST_PATH/phpunit.xml"
				fi

#				find */test/ -type f | while read file; do
#					sed -r \
#					    -i "$file"
#				done

#				quilt refresh
#				quilt diff

				cd ${TEST_PATH}
				echo_color "${Yellow}You have been chdir'ed to this location:${Color_Off}\n$(pwd)"
				echo
				echo_color "${Yellow}It is now time to test PHPUnit tests manually and possibly fix more things.${Color_Off}..."
				echo_color "${Yellow}Run tests with this cmdline: ${Color_Off}phpunit -v . | less"
				echo_color "${Yellow}Type '${Color_Off}exit 10${Yellow}' when done.${Color_Off}"

				bash || ret=$?
				if [ $ret = 10 ]; then
					echo_color "- ${Yellow}Continuing${Color_Off}"
					cd - 1>/dev/null
				else
					echo_color "- ${Yellow}Skipping to next package${Color_Off}"
					cd - 1>/dev/null
					continue
				fi

				quilt refresh
				quilt pop -a || true
				if [ -s debian/patches/$PATCH ]; then
					echo -e "Description: Adapt to PHP 8.x.\nAuthor: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n\n$(cat debian/patches/$PATCH)" > debian/patches/$PATCH
					git add debian/patches/*
					git commit debian/patches/series debian/patches/$PATCH -m "d/patches: Add $PATCH. Fix tests with PHP 8.x."
				fi
				git reset --hard
				git clean -df

#				if [ -e debian/tests/control ]; then
#					git checkout debian/tests/control
#					sed -r \
#					    -i debian/tests/control \
#					    -e "s/php-horde-test(,|$)/php-horde-test (>= 2.6.4+debian0-6~)\\1/"
#					git commit debian/tests/control -m "d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~)."
#				fi

			fi
			unset QUILT_PATCHES
			echo
			;;


		"fix-phpunit-tests")

			cat /etc/debian_version | grep -q "/sid" || {
				echo_color "${Red}You must be in a Debian testing/sid chroot/system to fix PHPUnit tests. Aborting...${Color_Off}"
				echo
				exit 1
			}

			find */test -maxdepth 0 -type d 1>/dev/null 2>/dev/null || {
				echo_color "${Yellow}No PHPUnit tests found in ${pear_name}.${Color_Off}"
				echo
				continue
			}

			find Horde_*-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null || {

				# some exceptions
				if   find ansel-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find content-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find imp-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find ingo-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find kronolith-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find mnemo-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find nag-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find passwd-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find sesha-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find turba-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find wicked-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				elif find horde_lz4-*/ -maxdepth 0 -type d 1>/dev/null 2>/dev/null; then
					:
				else
					echo_color "${Yellow}Not a Horde component, investigate manually, please.${Color_Off}"
					echo
					continue
				fi

			}

			all_tests_php_file=$(find */test/ -type f -name AllTests.php | head -n1)
			if [ -z "$all_tests_php_file" ]; then
				echo_color "${Red}Could not detect base folder of unit tests, skipping to next package.${Color_Off}"
				continue
			fi
			TEST_PATH=$(dirname $all_tests_php_file)

			export QUILT_PATCHES="$(pwd)/debian/patches"
			PATCH=1010_phpunit-8.x+9.x.patch
			if [ -e "debian/patches/series" ]; then
				quilt push -a  1>/dev/null|| true
			fi
			cd ${TEST_PATH}
			if phpunit .; then
				echo_color "${Green}PHPUnit tests succeed, continuing with next package.${Color_Off}"
				cd - 1>/dev/null
				quilt pop -a -f 1>/dev/null || true
				echo
				continue
			elif ( [ -e debian/patches/series ] && ( [ -e "debian/patches/$PATCH" ] && [ -s "debian/patches/$PATCH" ] ) ); then
				echo_color "${Red}PHPUnit patch for ${Color_Off}$pear_name${Red} is already present, but tests fail!!!${Color_Off}"
				echo_color "${Yellow}Cannot auto-fix PHPUnit tests.\nPlease investigate manually...${Color_Off}"
				read -p "$(echo_color ${Yellow}Do you want to work on ${Red}${pear_name}${Yellow}\'s PHPUnit tests now? ${Color_Off})" -n 1 -r
				echo
				if ! [[ $REPLY =~ ^[Yy]$ ]]; then
					cd - 1>/dev/null
					continue
				fi
				echo
				echo_color "${Yellow}You have been chdir'ed to this location:${Color_Off}\n$(pwd)"
				echo
				echo_color "${Yellow}Run PHPUnit tests manually and possibly fix more thing.${Color_Off}..."
				echo_color "${Yellow}Make sure to properly commit all your changes to Git before leaving.${Color_Off}"
				echo_color "${Yellow}Run tests with this cmdline: ${Color_Off}phpunit -v . | less"
				echo_color "${Yellow}Type '${Color_Off}exit 10${Yellow}' when done.${Color_Off}"

				bash || ret=$?
				if [ $ret = 10 ]; then
					echo_color "- ${Yellow}Continuing${Color_Off}"
					cd - 1>/dev/null
				else
					echo_color "- ${Yellow}Skipping to next package${Color_Off}"
					cd - 1>/dev/null
					continue
				fi

				cd - 1>/dev/null
				quilt pop -a -f 1>/dev/null || true
				echo
			else
				read -p "$(echo_color ${Yellow}Do you want to work on ${Red}${pear_name}${Yellow}\'s PHPUnit tests now? ${Color_Off})" -n 1 -r
				echo
				if ! [[ $REPLY =~ ^[Yy]$ ]]; then
					echo
					cd - 1>/dev/null
					continue
				fi
				cd - 1>/dev/null

				git reset --hard
				git clean -df

				if [ -e "$TEST_PATH/phpunit.xml" ]; then
					touch "$TEST_PATH/phpunit.xml"
				fi
				if [ ! -d debian/patches ]; then
					mkdir -p debian/patches
				fi

				export QUILT_PATCHES="$(pwd)/debian/patches"
				quilt push -a || true
				quilt new $PATCH
				quilt add $(find */test/ -type f)
				if [ ! -s "$TEST_PATH/phpunit.xml" ]; then
					quilt add "$TEST_PATH/phpunit.xml"
				fi

				find */test/ -type f | while read file; do
					sed -r \
					    -e s@"function setUp\(\)$"@"function setUp\(\): void"@g \
					    -e s@"function tearDown\(\)$"@"function tearDown\(\): void"@g \
					    -e s@"function setUpBeforeClass\(\)$"@"function setUpBeforeClass\(\): void"@g \
					    -e s@"function tearDownAfterClass\(\)$"@"function tearDownAfterClass\(\): void"@ \
					    -e s@"->setExpectedException\("@"->expectException\("@g \
					    -e s@"extends PHPUnit_Framework_TestCase"@"extends Horde_Test_Case"@g \
					    -e s@"assertInternalType\('string',\s*"@"assertIsString\("@g \
					    -e s@"assertInternalType\('array',\s*"@"assertIsArray\("@g \
					    -e s@"assertInternalType\('int',\s*"@"assertIsInt\("@g \
					    -e s@"assertInternalType\('object',\s*"@"assertIsObject\("@g \
					    -e s@"assertInternalType\('resource',\s*"@"assertIsResource\("@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)'\)"@"this->getMockBuilder('\1')->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', array\(\), array\(\), '', false, false\)"@"this->getMockBuilder('\1')->disableOriginalConstructor()->disableOriginalClone()->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', array\(\), array\(\), '', false\)"@"this->getMockBuilder('\1')->disableOriginalConstructor()->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', array\(\), (array\(.+\))\)"@"this->getMockBuilder('\1')->setConstructorArgs(\2)->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', (array\(.+\))\)"@"this->getMockBuilder('\1')->setMethods(\2)->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', (array\(.+\)), (array\(.+\))\)"@"this->getMockBuilder('\1')->setMethods(\2)->setConstructorArgs(\3)->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', (array\(.+\)), array\(\), '', false, false\)"@"this->getMockBuilder('\1')->setMethods(\2)->disableOriginalConstructor()->disableOriginalClone()->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', array\(\), (array\(.+\)), '', false, false\)"@"this->getMockBuilder('\1')->setConstructorArgs(\2)->disableOriginalConstructor()->disableOriginalClone()->getMock()"@g \
					    -e s@"this->getMock\('([\_A-Za-z]+)', (array\(.+\)), (array\(.+\)), '', false, false\)"@"this->getMockBuilder('\1')->setMethods(\2)->setConstructorArgs(\3)->disableOriginalConstructor()->disableOriginalClone()->getMock()"@g \
					    -i "$file"
				done

				if [ ! -s "$TEST_PATH/phpunit.xml" ]; then
					cp /usr/share/doc/php-horde-test/template/packagetest/phpunit.xml "$TEST_PATH/phpunit.xml"
				fi

				quilt refresh
				quilt diff

				cd ${TEST_PATH}
				echo_color "${Yellow}You have been chdir'ed to this location:${Color_Off}\n$(pwd)"
				echo
				echo_color "${Yellow}It is now time to test PHPUnit tests manually and possibly fix more thing.${Color_Off}..."
				echo_color "${Yellow}Run tests with this cmdline: ${Color_Off}phpunit -v . | less"
				echo_color "${Yellow}Type '${Color_Off}exit 10${Yellow}' when done.${Color_Off}"

				bash || ret=$?
				if [ $ret = 10 ]; then
					echo_color "- ${Yellow}Continuing${Color_Off}"
					cd - 1>/dev/null
				else
					echo_color "- ${Yellow}Skipping to next package${Color_Off}"
					cd - 1>/dev/null
					continue
				fi

				quilt refresh
				quilt pop -a || true
				if [ -s debian/patches/$PATCH ]; then
					echo -e "Description: Adapt to PHPUnit 8.x and 9.x API.\nAuthor: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n\n$(cat debian/patches/$PATCH)" > debian/patches/$PATCH
					git add debian/patches/*
					git commit debian/patches/series debian/patches/$PATCH -m "d/patches: Add $PATCH. Fix tests with PHPUnit 8.x/9.x."
				fi
				git reset --hard
				git clean -df

				if [ -e debian/tests/control ]; then
					git checkout debian/tests/control
					sed -r \
					    -i debian/tests/control \
					    -e "s/php-horde-test(,|$)/php-horde-test (>= 2.6.4+debian0-6~)\\1/"
					git commit debian/tests/control -m "d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~)."
				fi

			fi
			unset QUILT_PATCHES
			echo
			;;

		"edit-copyright")
			editor debian/copyright
			if [ -n "$(PAGER=cat git diff debian/copyright)" ]; then
				PAGER=cat git diff
				read -p "Are you sure (git commit debian/copyright -m \"d/copyright: Update copyright attributions.\")? " -n 1 -r
				echo
				if [[ $REPLY =~ ^[Yy]$ ]]; then
					git commit debian/copyright -m "d/copyright: Update copyright attributions."
				fi
			else
				echo_color "${Yellow}nothing changed, nothing to commit${Color_Off}"
			fi
			echo
			;;

		"close-itp")
			ITP=$(cat ../tools/horde-reupload-itps.csv | grep -E "^$debian_name,#" | cut -d "," -f2)
			if grep -q "$ITP" < debian/changelog; then
				echo_color "${Yellow}ITP $ITP is already being closed in debian/changelog for package ${debian_name}${Color_Off}"
			else
				awk "1;/^$/ && !x { print \"  * Re-upload to Debian. (Closes: $ITP).\"; print; x=1 }" debian/changelog > debian/changelog.new
				if [ -e "debian/changelog.new" ]; then
					mv debian/changelog.new debian/changelog
					PAGER=cat git diff
					read -p "Are you sure (git commit debian/changelog -m \"d/changelog: add re-upload ITP closure ($ITP)\")? " -n 1 -r
					echo
					if [[ $REPLY =~ ^[Yy]$ ]]; then
						git commit debian/changelog -m "d/changelog: add re-upload ITP closure ($ITP)"
					else
						git checkout debian/changelog
					fi
				fi
			fi
			echo
			;;

		"get-debian-origtarball")
			uversion=$(dpkg-parsechangelog | grep -E "^Version:" | awk '{ print $2 }' | cut -d"-" -f1)
			drevision=$(dpkg-parsechangelog | grep -E "^Version:" | awk '{ print $2 }' | cut -d"-" -f2)
			if [ -e ../${debian_name}_${uversion}.orig.tar.gz ]; then
				echo_color "${Yellow}orig tarball already downloaded from Debian archive${Color_Off}"
			else
				if [ "$drevision" != "1" ]; then
					wget http://deb.debian.org/debian/pool/main/p/${debian_name}/${debian_name}_${uversion}.orig.tar.gz -O ../${debian_name}_${uversion}.orig.tar.gz
				else
					echo_color "${Red}new upstream release upload pending, orig tarball not yet in Debian${Color_Off}"
				fi
			fi
			echo
			;;

		"update-changelog")
			if [ -n "$(git show HEAD debian/changelog)" ]; then
				echo_color "${Yellow}changelog has just been updated${Color_Off}"
			else
				gbp dch
				PAGER=cat git diff debian/changelog
				echo
				read -p "Are you sure (git commit debian/changelog -m \"d/changelog: update from Git history\")? " -n 1 -r
				echo
				if [[ $REPLY =~ ^[Yy]$ ]]; then
					git commit debian/changelog -m "d/changelog: update from Git history"
				fi
			fi
			echo
			;;

		"show-upstream-import")
			drevision=$(dpkg-parsechangelog | grep -E "^Version:" | awk '{ print $2 }' | cut -d"-" -f2)
			if [ "$drevision" == "1" ]; then
				upstream_branch="$(get_branch "upstream")"
				PAGER=less git show ${upstream_branch}
			else
				echo_color "${Yellow}skipping... (no recent upstream import)${Color_Off}"
			fi
			echo
			;;

		"edit-copyright-after-upstream-import")
			drevision=$(dpkg-parsechangelog | grep -E "^Version:" | awk '{ print $2 }' | cut -d"-" -f2)
			if [ "$drevision" == "1" ]; then
				editor debian/copyright
				if [ -n "$(PAGER=cat git diff debian/copyright)" ]; then
					PAGER=cat git diff
					read -p "Are you sure (git commit debian/copyright -m \"d/copyright: Update copyright attributions.\")? " -n 1 -r
					echo
					if [[ $REPLY =~ ^[Yy]$ ]]; then
						git commit debian/copyright -m "d/copyright: Update copyright attributions."
					fi
				else
					echo_color "${Yellow}nothing changed, nothing to commit${Color_Off}"
				fi
			fi
			echo
			;;


		"quilt")
			export QUILT_PATCHES=$(pwd)/debian/patches

			if [ -e ${QUILT_PATCHES}/series ]; then

				case "$value" in
					"push")
						quilt push $quilt_options || true
					;;
					"pop")
						quilt pop $quilt_options || true
						if [ "${quilt_options}" = "-a" ]; then
							rm .pc/ -Rfv
						fi
					;;
					"diff")
						quilt diff $quilt_options || true
					;;
					"refresh")
						quilt refresh $quilt_options || true
					;;
				esac
			else
				echo_color "${Yellow}No patches series applied to this package.${Color_Off}"
			fi
			echo
			unset QUILT_PATCHES
			;;

		"git")
			case "$value" in
				"show")
					PAGER=cat git show $git_options
					;;
				"show-pager")
					PAGER=cat git -p show $git_options | less
					;;
				"log")
					PAGER=less git log $git_options || true
					;;
				"diff")
					PAGER=cat git diff $git_options
					;;
				"status")
					PAGER=cat git status $git_options
					;;
				"rebase")
					PAGER=cat git rebase $git_options
					;;
				"push")
					git push
					;;
			        "pull")
					git pull
					;;
				"commit")
					git commit $git_options
					;;
				"revert")
					PAGER=cat git show $git_options
					read -p "Are you sure (git revert $git_options)? " -n 1 -r
					echo
					if [[ $REPLY =~ ^[Yy]$ ]]; then
						git revert $git_options
					fi
					;;
				"reset")
					echo $git_options
					PAGER=cat git diff
					PAGER=cat git show
					read -p "Are you sure (git reset $git_options)? " -n 1 -r
					echo
					if [[ $REPLY =~ ^[Yy]$ ]]; then
						git reset $git_options
					fi
					;;
				"clean")
					PAGER=cat git status
					read -p "Are you sure (git clean $git_options)? " -n 1 -r
					echo
					if [[ $REPLY =~ ^[Yy]$ ]]; then
						git clean $git_options
					fi
					;;
				*)
					echo_color "${Red}git command '$value' not supported${Color_Off}"
					;;
			esac
			echo
			;;

		"gbp")
			case "$value" in

				"tag")
					gbp tag || true
					;;
			esac
			echo
			;;

		*)
			echo_color "${Red}no such action: '$action'${Color_Off}"
			;;
	esac

done 7< "$pear_name_file"
