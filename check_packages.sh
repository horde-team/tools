#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping$Color_Off"
        continue
    fi
	debian_src_name=$(echo "${debian_name}" | sed 's/^php5-/php-/')
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
    echo_color "$Green============------------$Color_Off"
    if [ ! -d "${git_dir}/.git" ]; then
        echo_color "- ${Yellow}Git dir not present. Skipping to next package${Color_Off}"
        continue
    fi
    cd $git_dir
    modified_files_count=$(git status -s | wc -l)
    if [ $modified_files_count -gt 0 ]; then
        echo_color "- ${Yellow}Some local changes are not commited:${Color_Off}"
        git status
        echo_color "- ${Yellow}Skipping to next package${Color_Off}"
        continue
    fi
    ret=0
    git fetch origin || ret=$?
    if [ $ret != 0 ]; then
        echo_color "- ${Yellow}Unable to fetch Git origin. Ignoring.${Color_Off}"
        continue
    fi
	debian_branch=$(get_branch 'debian')
	upstream_branch=$(get_branch 'upstream')
    for branch in "${debian_branch}" "${upstream_branch}" "${upstream_branch}+repack"; do
        local_rev=$(git rev-parse --revs-only $branch)
        origin_rev=$(git rev-parse --revs-only origin/$branch)
        if [ "$local_rev" != "" -a "$origin_rev" = "" ]; then
            git checkout $branch
            echo_color "- ${Yellow}Upstream branch not found, pushing${Color_Off}"
            git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
            git push --set-upstream origin "$branch"
            origin_rev=$(git rev-parse --revs-only origin/$branch)
        fi
        if [ "$local_rev" = "" -a "$origin_rev" != "" ]; then
            echo_color "- ${Yellow}Local branch not found, pulling${Color_Off}"
            git checkout $branch
        fi
        if [ "$local_rev" != "$origin_rev" ]; then
            echo_color "- ${Yellow}Branch $branch is not in sync (local:$local_rev, origin:$origin_rev)${Color_Off}"
            git checkout $branch
            read -p 'What do you want to do: "rebase" (interactively, then push); "merge" (then push); "push"; "skip")? ' branch_action
            if [ "${branch_action}" = "rebase" ]; then
                git rebase -i "origin/$branch"
                git push
            elif [ "${branch_action}" = "merge" ]; then
                git merge "origin/$branch"
                git push
            elif [ "${branch_action}" = "push" ]; then
                git push
            else
                echo_color "- ${Yellow}Doing nothing${Color_Off}"
            fi
        fi
    done
    git checkout -q "${debian_branch}"
    if head -n 1 debian/changelog | grep -q UNRELEASED; then
        echo_color "- ${Yellow}changelog refers to the UNRELEASED distribution:${Color_Off}"
        dpkg-parsechangelog
        if [ "${auto_dch}" = "yes" ]; then
            confirm_dch=yes
            echo_color "- ${Green}Auto: 'dch --release'${Color_Off}"
        else
            read -p 'Do you want to "dch --release" (type "yes")? ' confirm_dch
        fi
        if [ "${confirm_dch}" = "yes" ]; then
            dversion=`dpkg-parsechangelog | sed -rne 's/^Version: (.*)$/\1/p'`
            previous_major=`dpkg-parsechangelog --offset 1 --count 1 | sed -rne 's/^Version: ([0-9]+)[.].*$/\1/p'`
            current_major=`dpkg-parsechangelog                       | sed -rne 's/^Version: ([0-9]+)[.].*$/\1/p'`
            dch_args=''
            distribution=unstable
            if [ "${current_major}" != "${previous_major}" ]; then
                distribution=experimental
                dch_args="$dch_args --distribution ${distribution}"
            fi
            if [ "${auto_dch}" = "yes" ]; then
                dch --release '' $dch_args
            else
                dch --release $dch_args
            fi
            ret=0
            git commit 'debian/changelog' -m"upload to ${distribution} (debian/${dversion})" || ret=$?
            if [ $ret != 0 ]; then
                echo_color "- ${Yellow}No changes, skipping to next package.${Color_Off}"
                continue
            fi
        else
            echo_color "- ${Yellow}Skipping to next package${Color_Off}"
            continue
        fi
    else
        #echo_color "- ${Green}Package already released. Skipping to next package${Color_Off}"
        continue
    fi
    echo_color "- ${Green}Rebuilding...${Color_Off}"
    ret=0
    gbp buildpackage --no-sign > /dev/null || ret=$?
    if [ $ret != 0 ]; then
        echo_color "- ${Red}Build failed. Starting a bash session, exit 10 if you have fixed and rebuild.${Color_Off}"
        bash || ret=$?
        if [ $ret = 10 ]; then
            echo_color "- ${Yellow}Continuing${Color_Off}"
        else
            echo_color "- ${Yellow}Skipping to next package${Color_Off}"
            continue
        fi
    fi
    pkg_version=$(dpkg-parsechangelog --format rfc822 | grep '^Version:' | sed 's/Version: //')
    echo_color "- ${Green}Checking with Lintian...${Color_Off}"
    ret=0
    lintian --no-tag-display-limit --color always -I --pedantic --show-overrides "../build-area/${debian_src_name}_${pkg_version}_"*.changes || ret=$?
    if [ $ret != 0 ]; then
        echo_color "- ${Red}Lintian exited with return value $ret.${Color_Off}"
        if [ "${auto_ignore_lintian}" = "yes" ]; then
            ignore_lintian=yes
            echo_color "- ${Green}Auto: ignore_lintian${Color_Off}"
        else
            read -p "Do you want to ignore and proceed (type 'yes')? " ignore_lintian
        fi
        if [ "${ignore_lintian}" != "yes" ]; then
            echo_color "- ${Yellow}Skipping to next package${Color_Off}"
            continue
        fi
    fi
    if [ "${auto_dput}" = "yes" ]; then
        confirm_dput=yes
        echo_color "- ${Green}Auto: uploading version '${pkg_version}'${Color_Off}"
    else
        read -p "Do you want to upload version '${pkg_version}' (type 'yes')? " confirm_dput
    fi
    if [ "${confirm_dput}" = "yes" ]; then
        echo_color "- ${Green}Rebuilding source...${Color_Off}"
        ret=0
        gbp buildpackage --no-sign --git-no-pbuilder -S -d > /dev/null || ret=$?
        if [ $ret != 0 ]; then
            echo_color "- ${Red}Source build failed. Starting a bash session, exit 10 if you have fixed and rebuild.${Color_Off}"
            bash || ret=$?
            if [ $ret = 10 ]; then
                echo_color "- ${Yellow}Continuing${Color_Off}"
            else
                echo_color "- ${Yellow}Skipping to next package${Color_Off}"
                continue
            fi
        fi
        debsign "../build-area/${debian_src_name}_${pkg_version}_source.changes"
        dput "../build-area/${debian_src_name}_${pkg_version}_source.changes"
    else
        echo_color "- ${Yellow}Skipping to next package${Color_Off}"
        continue
    fi

    if [ "${auto_gitpush}" = "yes" ]; then
        confirm_gitpush=yes
        echo_color "- ${Green}Auto: pushing changes of version '${pkg_version}' to salsa.debian.org${Color_Off}"
    else
        read -p "Do you want to push changes of version '${pkg_version}' to salsa.debian.org (type 'yes')? " confirm_gitpush
    fi
    if [ "${confirm_gitpush}" = "yes" ]; then
        gbp tag
        gbp push
    else
        echo_color "- ${Yellow}Skipping to next package${Color_Off}"
        continue
    fi
    echo

done 7< "$pear_name_file"
