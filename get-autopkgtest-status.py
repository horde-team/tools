#!/usr/bin/python3

# Script to get the autopkgtest-status of packages.
# As debci does not provide a way to get the status of a package,
# we have to parse the RSS-feed of the package.

import argparse
import feedparser
from termcolor import colored

def get_status(packages):
    for package in packages:
        url = f"https://ci.debian.net/data/feeds/{package[0]}/{package}.xml"
        feed = feedparser.parse(url)

        msg = f"{package} : "
        if (len(feed.entries) == 0):
            if args.color:
                msg += colored("No autopkgtest results found.", "yellow")
            else:
                msg += "No autopkgtest results found."
        else:
            for entry in feed.entries:
                version = entry.title.split(' ')[1].strip()
                state = entry.title.split(' ')[2].strip().lower()
                suite = (entry.title.split(' ')[4].strip()).split('/')[0].strip()
                arch = (entry.title.split(' ')[4].strip()).split('/')[1].strip()

                if (args.debug):
                    print(
                        f"version: {version} state: {state} suite: {suite} arch: {arch}")

                if ((arch == 'amd64' or arch == 'i386' or arch == 'arm64') and suite == 'unstable'):
                    if state == 'pass':
                        color = 'green'
                    elif state == 'fail':
                        color = 'red'
                    if args.color:
                        msg += (f"    {version} {colored(state, color)}  {suite} {arch}")
                    else:
                        msg += (f"    {version} {state}  {suite} {arch}")
                    break
        if msg:
            print(msg)


if __name__ == "__main__":

    # Read the command line arguments
    parser = argparse.ArgumentParser(
        description='Get the autopkgtest status of packages.')
    parser.add_argument('-r', '--ring', default='ring0.list',
                        help='The ring file to use')
    parser.add_argument('-p', '--package', help='The package to check')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug output')
    parser.add_argument('-c', '--color', action='store_true',
                        help='Enable color output')

    args = parser.parse_args()


    packages = []


    if args.package:
        packages.append(args.package)
    else:
        # Read the ring file
        with open(args.ring, 'r') as f:
            file = f.read()

        for line in file.splitlines():
            if line.startswith('#'):
                continue
            package = 'php-'+line.split()[0].lower().replace('_', '-')
            packages.append(package)

    get_status(packages)

