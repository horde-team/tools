#!/bin/sh

set -x
set -e

image_exists() {
  local imagetag="$1"
  local ret=0
  docker inspect --format '.Container' "${imagetag}" > /dev/null 2>&1 || ret=$?
  return $ret
}

create_image_if_not_exists() {
  local imagetag="$1"
  local sourcedir="$2"
  if ! image_exists "${imagetag}" ; then
    echo "Creating image '${imagetag}' from '${sourcedir}'"
    docker build -t "${imagetag}" "${sourcedir}"
  fi
}

for image in horde_webmail dovecot; do
  create_image_if_not_exists "debian_horde/${image}" "./${image}"
done

common_opts='-d -v /dev/log:/dev/log'
docker run $common_opts -P       --name dovecot                   debian_horde/dovecot
docker run $common_opts -p 80:80 --name horde --link dovecot:imap debian_horde/horde_webmail
