<?php
$servers['imap'] = array(
    'name' => 'IMAP server',
    'hostspec' => 'imap',
    'hordeauth' => false,
    'protocol' => 'imap',
    'secure' => 'tls',
    //'debug' => '/tmp/imap.debug
);
