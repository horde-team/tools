#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
    echo_color "$Green$debian_name ($pear_name)$Color_Off"
    if [ ! -d "${git_dir}/.git" ]; then
        echo_color "- ${Yellow}Git dir not present. Skipping to next package${Color_Off}"
        continue
    fi
    cd $git_dir
    # Go to debian-sid branch
    debian_branch=$(get_branch 'debian')
    git checkout --quiet "${debian_branch}"
    old_sha="$(git rev-parse --verify HEAD)"
    lintian-brush
    PAGER=cat git log -p $old_sha..HEAD
#    git push
done 7< "$pear_name_file"
