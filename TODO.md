# General TODO
- [url](http://udd.debian.org/dmd.cgi?email1=pkg-horde-hackers@lists.alioth.debian.org)
- run tests during build
- Standards-Version
- Lintian errors
  - horde
  - Horde_Mime_Viewer
  - Horde_Scribe
- Other lintian warnings: http://lintian.debian.org/maintainer/pkg-horde-hackers@lists.alioth.debian.org.html

# ====================================
# Missing dependencies: external2
#DONE
* php5-msgpack (pecl.php.net msgpack)
* php5-eaccelerator (extension eaccelerator) => override none
* php-net-dns2 (pear.php.net Net_DNS2)
* php5-igbinary (pecl.php.net igbinary)
* php5-idn (pecl.php.net idn) => Provided by php5-intl (License=PHP: https://bugs.php.net/bug.php?* id=67142)
* php5-pecl-http (pecl.php.net pecl_http) => already in sid
* php5-ssh2 (pecl.php.net ssh2) => libssh2-php http://bugs.debian.org/635296
* php-text-languagedetect (pear.php.net Text_LanguageDetect)
* php-xml-svg (pear.php.net XML_SVG)
* php-console-getopt (pear.php.net Console_Getopt) => in php-pear. License=PHP-3.01: https://pear.php.net/bugs/20259

# WIP:
* php5-oci8 (extension oci8) => NEW (contrib -> suggests)
* php5-pam (pecl.php.net pam) => NEW (ITP http://bugs.debian.org/746219 License=PHP-3.01: https://bugs.php.net/bug.php?id=67139)
* php-file-fstab (pear.php.net File_Fstab) => NEW (ITP http://bugs.debian.org/738851 License=PHP-3.01: https://pear.php.net/bugs/20257)

# FIXME:
php-nrk-predis (pear.nrk.io Predis) => libphp-predis http://bugs.debian.org/739670

# TODO:
* php5-lzf (pecl.php.net lzf) => License=PHP-3.01: https://bugs.php.net/bug.php?id=67143
* php5-xdiff (pecl.php.net xdiff) => License=PHP-3.01: https://bugs.php.net/bug.php?id=67144

# BLOCKED
* php-date-holidays (pear.php.net Date_Holidays) => (ITP http://bugs.debian.org/775784 License=PHP-2.02!: https://pear.php.net/bugs/20258)
* php-file-find (pear.php.net File_Find) => License=PHP-2.02!: https://pear.php.net/bugs/20256

# ====================================
# Missing dependencies: external3
# DONE
* php5-com (extension com) => override none
* php-crypt-chap (pear.php.net Crypt_CHAP)
* php5-php-facedetect (extension php-facedetect) => NEW (ITP http://bugs.debian.org/775864)

# WIP:

# FIXME:
* php5-libpuzzle (extension libpuzzle) => libpuzzle-php

# TODO:
php-text-wiki-creole (pear.php.net Text_Wiki_Creole) => ITP http://bugs.debian.org/746290
php-text-wiki-mediawiki (pear.php.net Text_Wiki_Mediawiki) => ITP http://bugs.debian.org/577931
php-text-wiki-tiki (pear.php.net Text_Wiki_Tiki) => ITP http://bugs.debian.org/746292

# Not needed
php5-opencv (extension opencv) => NOTNEEDED=https://github.com/horde/horde/pull/119 License=PHP-3.01: https://github.com/mgdm/OpenCV-for-PHP/issues/6

# ====================================
# Notes from Anton 2022/2023
1. <2022-12-21 Mi> [php-horde-db](https://salsa.debian.org/horde-team/php-horde-db/-/blob/debian-sid/debian/patches/1012_php8.1.patch#L514) Not sure about this part of the patch.
1. <2022-12-24 Sa> [php-horde-kolab-storage](https://salsa.debian.org/horde-team/php-horde-kolab-storage/-/blob/debian-sid/debian/tests/control#L3) has allow-stderr. Should be fixed.
1. <2022-12-24 Sa> [php-horde-imap-client](https://salsa.debian.org/horde-team/php-horde-imap-client/-/blob/debian-sid/debian/tests/control#L3) has allow-stderr. Should be fixed.
1. <2022-12-25 Su> [php-horde-prefs](https://salsa.debian.org/horde-team/php-horde-prefs/-/blob/debian-sid/debian/tests/control#L3) has allow-stderr. Should be fixed. Patch changes Identity.php, should be checked.
1. <2022-12-31 Sat> php-horde-perms - drop allow-stderr
