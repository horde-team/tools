Prerequisites
=============
1. Install `pkg-php-tools`, `pear-channels` and `git-buildpackage`.

2. Follow http://www.horde.org/development/git to clone the horde repository
   and make development repo web-accessible.

3. Clone horde-team/tools.git:

       mkdir -p ~/src/salsa.debian.org/horde-team
       cd ~/src/salsa.debian.org/horde-team
       git clone git@salsa.debian.org:horde-team/tools.git
       cd tools

4. From there, copy `config.sh.dist` to `config.sh` and fill it with the same
   values as `install_dev.conf` (`horde_git`, `web_dir`) and `pkg_horde_dir`.

5. (optionnaly) Edit `$web_dir/components/data/distribute/debian/templatedir`.
   You can also use the debian-packaging branch from https://github.com/sathieu/horde.git

General considerations about scripts and "rings"
================================================

Most scripts takes a file as only parameter, this file should contain a list of
Horde's PEAR package names.

Several "rings" are predefined (each ring is around the previous):
  * `ring0.list`: list of PEAR packages recursively depended on by Horde_Core.
  * `ring1.list`: list of PEAR packages recursively depended on by groupware and
    webmail.
  * `ring2.list`: list of PEAR packages recursively recommended by groupware and
    webmail.
  * `ring3.list`: list of PEAR packages recursively recommended by ansel, gollem,
    passwd, sesha, whups, wicked.
  * `ring4.list`: All remaining PEAR packages (with a package.xml)

Those list can be updated by running:

    ./update_ring.sh <file.list>|all

Pulling existing packages
=========================

    ./pull_packages.sh <file.list>

Creating new packages
=====================

    ./bootstrap_packages.sh <file.list>

Building packages
=================

    ./build_packages.sh <file.list>

Updating packages
=================
To check and integrate new upstream releases, run:

    ./update_packages.sh <file.list>

Checking and uploading packages
==============================

    ./check_packages.sh <file.list>


HowTos
==============================

Some important howtos are being documented here

## Autopkgtests with the database

Some autopkgtests require the access to the database to be executed.
There is an example, how it can be achieved with the docker.
The idea is to start 1 container with mysql, another with postgresql
and expose the socket to the third docker container, where the
autopkgtests should be executed.

Setup the mariadb server and expose the socket folder

```
docker run --name mysqlbtest -e MYSQL_ALLOW_EMPTY_PASSWORD=1 --rm -v /tmp/mysql-socket:/var/run/mysqld -p 3306:3306 mysql:8.0
```

Setup the postgresql server and expose the socket folder
```
docker run --name postgretest -e POSTGRES_HOST_AUTH_METHOD=trust --rm -v /tmp/postgre-socket:/var/run/postgresql postgres:latest
```

For postgresql v14 the container postgres:14.6-alpine and for v15 postgres:15.1-alpine can be used.

Run the autopkgtest  container

```
docker run --rm -it -v /tmp/mysql-socket:/run/mysqld -v /tmp/mysql-socket:/var/run/mysqld  -v /tmp/postgre-socket:/var/run/postgresql --network=host debian:sid_mysqlcient
```

To stop the database containers. execute:

```
docker stop mysqlbtest && docker stop postgretest
```


## Autopkgtests with the ldap (php-horde-ldap)

Install slapd on the host system, using the debian/tests/phpunit from the package php-horde-ldap,
dropping the last part, starting from "cd Horde....".
```
sudo sh debian/tests/phpunit
```

Start the docker container, letting the docker container use host network.

```
docker run --rm -it  --network=host debian:sid
```

Then autopkgtest use the host ldap DB.
