#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping$Color_Off"
        continue
    fi
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	git_remote=$(get_git_remote_url "${pear_channel}" "${pear_name}" "${debian_name}")
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
    if [ -e "$git_dir" ]; then
        echo_color "- ${Green}Already exists, fetching...${Color_Off}"
        cd "$git_dir"
        git fetch origin
        echo_color "- ${Green}and rebasing...${Color_Off}"
        for branch in pristine-tar upstream debian; do
        	branch=$(get_branch "${branch}")
		    git checkout -q "${branch}"
		    git rebase -q --stat origin/"${branch}"
	    done
        continue
    else
        echo_color "- ${Green}cloning into '$git_dir'...${Color_Off}"
        if git clone "${git_remote}" "${git_dir}"; then
            cd "${git_dir}"
		    for branch in pristine-tar upstream debian; do
		    	branch=$(get_branch "${branch}")
				git checkout "${branch}"
			done
        else
            echo_color "- ${Red}Error: Skipping to next package...${Color_Off}"
            continue
        fi
    fi
done 7< "$pear_name_file"
