#!/bin/bash

echo "FATAL: need to port this script to use salsa's gitlab API"
exit 1

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping$Color_Off"
        continue
    fi
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	git_remote=$(get_git_remote_url "${pear_channel}" "${pear_name}" "${debian_name}")
	debian_src_name=$(echo "${debian_name}" | sed 's/^php5-/php-/')
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
	if [ -e "$git_dir" ]; then
		echo_color "- ${Green}Already exists locally. Skipping to next package${Color_Off}"
		continue
	fi
	if deb_exists sid "${debian_name}"; then
		echo_color "- ${Green}Already exists in sid. Skipping to next package${Color_Off}"
		continue
	fi
	maintainer='Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>'
	watch_line=''
	if [ "${pear_channel}" = 'pear.horde.org' ]; then
		maintainer='Horde Maintainers <pkg-horde-hackers@lists.alioth.debian.org>'
		ret=0
		do_horde_components_distribute "$pear_name" "$pkg_horde_build_dir/$pear_name" || ret=$?
		if [ $ret != 0 ]; then
			echo_color "- ${Red}failed!${Color_Off}"
			echo_color "- ${Yellow}Skipping to next package${Color_Off}"
			continue
		fi
	else
		if [ -f "$pkg_horde_build_dir/$pear_name" ]; then
			rm -rf "$pkg_horde_build_dir/$pear_name"
		fi
		mkdir -p "$pkg_horde_build_dir/$pear_name"
		cd "$pkg_horde_build_dir/$pear_name"
		if [ "${pear_channel}" = 'extension' -o "${pear_channel}" = 'pecl.php.net' ]; then
			maintainer='Debian PHP PECL Maintainers <pkg-php-pecl@lists.alioth.debian.org>'
			url=http://pecl.php.net/package/${pear_name}
			watch_line="http://pecl.php.net/package/${pear_name}/download /get/${pear_name}-(.*).tgz"
			for state in stable beta alpha devel snapshot; do
				ret=0
				pecl -q -d "preferred_state=${state}" download "${pear_name}.tgz" || ret=$?
				if [ $ret = 0 ]; then
					break
				fi
			done
			if [ $ret != 0 ]; then
				echo_color "- ${Red}Download failed!${Color_Off}"
				echo_color "- ${Yellow}Skipping to next package${Color_Off}"
				continue
			fi
			upstream_version=$(ls *.tgz | sed "s/${pear_name}-\(.*\).tgz/\1/")
			mv -v *.tgz "${debian_src_name}_${upstream_version}.orig.tar.gz"
		elif [ "${pear_channel}" = 'pear.php.net' ]; then
			url=http://pear.php.net/package/${pear_name}
			watch_line="http://pear.php.net/package/${pear_name}/download http://download.pear.php.net/package/${pear_name}-(.*).tgz"
			for state in stable beta alpha devel snapshot; do
				ret=0
				pear -q -d "preferred_state=${state}" download "${pear_name}.tgz" || ret=$?
				if [ $ret = 0 ]; then
					break
				fi
			done
			if [ $ret != 0 ]; then
				echo_color "- ${Red}Download failed!${Color_Off}"
				echo_color "- ${Yellow}Skipping to next package${Color_Off}"
				continue
			fi
			upstream_version=$(ls *.tgz | sed "s/${pear_name}-\(.*\).tgz/\1/")
			mv -v *.tgz "${debian_src_name}_${upstream_version}.orig.tar.gz"
		else
			# FIXME channel-discover
			echo_color "- ${Red}Not implemented!${Color_Off}"
			echo_color "- ${Yellow}Skipping to next package${Color_Off}"
			continue
		fi
	fi
	mkdir "$git_dir"
	cd "$git_dir"
	git init
	git symbolic-ref HEAD refs/heads/upstream-sid
	tar -xvzf "$pkg_horde_build_dir/$pear_name/${debian_src_name}_"*.orig.tar.gz
	git add *
	git commit -m'Initial release'
	upstream_version=$(pkgtools phppear version)
	debian_upstream_version=$(pkgtools phppear debianversion "$upstream_version" | sed 's/~/_/g')
	git tag "upstream/${debian_upstream_version}"
	pristine-tar commit "$pkg_horde_build_dir/$pear_name/${debian_src_name}_"*.orig.tar.gz "upstream/${debian_upstream_version}"
	git checkout -b debian-sid
	git rebase upstream-sid
	if [ "${pear_channel}" = 'pear.horde.org' ]; then
		if [ -f "$pkg_horde_build_dir/$pear_name/${debian_src_name}_"*.debian.tar.xz ]; then
			tar -xvJf "$pkg_horde_build_dir/$pear_name/${debian_src_name}_"*.debian.tar.xz
		else
			tar -xvzf "$pkg_horde_build_dir/$pear_name/${debian_src_name}_"*.debian.tar.gz
		fi
	else
		bdeps='debhelper (>= 9), pkg-php-tools'
		deps='${misc:Depends}, ${phppear:Debian-Depends}'
		dh_options='--buildsystem=phppear --with phppear'
		arch='all'
		if [ "$(pkgtools phppear packagetype)" = 'extsrc' ]; then
			bdeps="${bdeps} (>= 1.5~), php5-dev, dh-php5"
			deps="\${shlibs:Depends}, ${deps}"
			dh_options="${dh_options} --with php5"
			arch='any'
		fi
		mkdir -p debian/source
		echo '3.0 (quilt)' > debian/source/format
		echo '9' > debian/compat
		dch --create --package "${debian_src_name}" --distribution UNRELEASED --urgency medium -v "${upstream_version}-1" ''
		# dch get the uploader right, take it from d/changelog
		uploader=$(grep '^ --' debian/changelog | sed 's/^ -- \([^>]\+>\).*/\1/')
		cat <<EOF > debian/control
Source: ${debian_src_name}
Section: php
Priority: optional
Maintainer: ${maintainer}
Uploaders: ${uploader}
Build-Depends: ${bdeps}
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/horde-team/${debian_src_name}
Vcs-Browser: https://salsa.debian.org/horde-team/${debian_src_name}.git
Homepage: ${url}

Package: ${debian_name}
Architecture: ${arch}
Depends: ${deps}
Recommends: \${phppear:Debian-Recommends}
Breaks: \${phppear:Debian-Breaks}
Description: \${phppear:summary}
 \${phppear:description}
EOF

		cat <<EOF > debian/rules
#!/usr/bin/make -f
%:
	dh \$@ ${dh_options}
EOF
		chmod 0755 debian/rules
		if [ -n "${watch_line}" ]; then
			cat <<EOF > debian/watch
version=3
${watch_line}
EOF
		fi
		# PECL
		if [ "$(pkgtools phppear packagetype)" = 'extsrc' ]; then
			echo "mod debian/${pear_name}.ini" > debian/php5
			cat <<EOF > "debian/${pear_name}.ini"
; configuration for php ${pear_name} module
; priority=20
extension=${pear_name}.so
EOF
		fi
	fi
	git add *
	git commit -m'Initial packaging'
	git config remote.origin.url "${git_remote}"
	git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
	git_remote_dir=$(get_git_remote_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	ssh -n git.debian.org mkdir "${git_remote_dir}"
	ssh -n git.debian.org git init --bare "${git_remote_dir}"
	ssh -n git.debian.org "GIT_DIR='${git_remote_dir}' git symbolic-ref HEAD refs/heads/debian-sid"
	git checkout pristine-tar && git push --set-upstream origin pristine-tar
	git checkout upstream-sid && git push --set-upstream origin upstream-sid
	git checkout debian-sid   && git push --set-upstream origin debian-sid
	# gbp
	git checkout debian-sid
	cat <<EOF > debian/gbp.conf
[DEFAULT]
pristine-tar = True
upstream-branch = upstream-sid
debian-branch = debian-sid

[buildpackage]
export-dir = ../build-area/
EOF
	git add debian/gbp.conf
	git commit -m"git-buildpackage configuration"
	git push
	git push --tags
done 7< "$pear_name_file"
