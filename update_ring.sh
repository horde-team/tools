#!/bin/bash

set -e

. `dirname $0`/functions.sh

case $1 in
  ring0.list)
    echo Horde_Role > ring0.list.tmp
    get_dependency_list Horde_Core >> ring0.list.tmp
    grep -v '^Horde_Core$' ring0.list.tmp > ring0.list.tmp2
    echo 'Horde_Core' >> ring0.list.tmp2
    rm ring0.list.tmp
    mv ring0.list.tmp2 ring0.list
    ;;
  ring1.list)
    get_dependency_list webmail > ring1.list.tmp
    get_dependency_list groupware >> ring1.list.tmp
    awk ' !x[$0]++' ring1.list.tmp > ring1.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="ring0.list" { ring0[$0]++}
        FILENAME=="ring1.list.tmp2" { if (ring0[$0]==0) {print $0} }
        ' ring0.list ring1.list.tmp2 > ring1.list.tmp3 # remove ring0
    rm ring1.list.tmp ring1.list.tmp2
    mv ring1.list.tmp3 ring1.list
    ;;
  ring2.list)
    get_dependency_list webmail --alldeps > ring2.list.tmp
    get_dependency_list groupware --alldeps >> ring2.list.tmp
    awk ' !x[$0]++' ring2.list.tmp > ring2.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="ring0.list" { ring0[$0]++}
        FILENAME=="ring1.list" { ring1[$0]++}
        FILENAME=="ring2.list.tmp2" { if (ring0[$0]==0 && ring1[$0]==0) {print $0} }
        ' ring0.list ring1.list ring2.list.tmp2 > ring2.list.tmp3 # remove ring0 and ring1
    rm ring2.list.tmp ring2.list.tmp2
    mv ring2.list.tmp3 ring2.list
    ;;
  ring3.list)
    get_dependency_list ansel --alldeps > ring3.list.tmp
    get_dependency_list gollem --alldeps >> ring3.list.tmp
    get_dependency_list passwd --alldeps >> ring3.list.tmp
    get_dependency_list sesha --alldeps >> ring3.list.tmp
    get_dependency_list whups --alldeps >> ring3.list.tmp
    get_dependency_list wicked --alldeps >> ring3.list.tmp
    awk ' !x[$0]++' ring3.list.tmp > ring3.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="ring0.list" { ring0[$0]++}
        FILENAME=="ring1.list" { ring1[$0]++}
        FILENAME=="ring2.list" { ring2[$0]++}
        FILENAME=="ring3.list.tmp2" { if (ring0[$0]==0 && ring1[$0]==0 && ring2[$0]==0) {print $0} }
        ' ring0.list ring1.list ring2.list ring3.list.tmp2 > ring3.list.tmp3 # remove ring0 and ring1
    rm ring3.list.tmp ring3.list.tmp2
    mv ring3.list.tmp3 ring3.list
    ;;
  ring4.list)
    echo -n > ring4.list.tmp
    for p in "$horde_git"/framework/*/package.xml "$horde_git"/*/package.xml "$horde_git"/bundles/*/package.xml; do
      pkgtools --sourcedirectory "$(dirname $p)" phppear name >> ring4.list.tmp
      echo >> ring4.list.tmp
    done
    awk '
        FILENAME=="ring0.list" { ring0[$0]++}
        FILENAME=="ring1.list" { ring1[$0]++}
        FILENAME=="ring2.list" { ring2[$0]++}
        FILENAME=="ring3.list" { ring3[$0]++}
        FILENAME=="ring4.list.tmp" { if (ring0[$0]==0 && ring1[$0]==0 && ring2[$0]==0 && ring3[$0]==0) {print $0} }
        ' ring0.list ring1.list ring2.list ring3.list ring4.list.tmp > ring4.list.tmp2 # remove ring0, ring1 and ring2
    rm ring4.list.tmp
    mv ring4.list.tmp2 ring4.list
    ;;
  external0.list)
    echo -n > external0.list
    get_external_dependency_list Horde_Core >> external0.list
    ;;
  external1.list)
    get_external_dependency_list webmail > external1.list.tmp
    get_external_dependency_list groupware >> external1.list.tmp
    awk ' !x[$0]++' external1.list.tmp > external1.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="external0.list" { external0[$0]++}
        FILENAME=="external1.list.tmp2" { if (external0[$0]==0) {print $0} }
        ' external0.list external1.list.tmp2 > external1.list # remove external0
    rm external1.list.tmp external1.list.tmp2
    ;;
  external2.list)
    get_external_dependency_list webmail --alldeps > external2.list.tmp
    get_external_dependency_list groupware --alldeps >> external2.list.tmp
    awk ' !x[$0]++' external2.list.tmp > external2.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="external0.list" { external0[$0]++}
        FILENAME=="external1.list" { external1[$0]++}
        FILENAME=="external2.list.tmp2" { if (external0[$0]==0 && external1[$0]==0) {print $0} }
        ' external0.list external1.list external2.list.tmp2 > external2.list # remove external0 and external1
    rm external2.list.tmp external2.list.tmp2
    ;;
  external3.list)
    get_external_dependency_list ansel --alldeps > external3.list.tmp
    get_external_dependency_list gollem --alldeps >> external3.list.tmp
    get_external_dependency_list passwd --alldeps >> external3.list.tmp
    get_external_dependency_list sesha --alldeps >> external3.list.tmp
    get_external_dependency_list whups --alldeps >> external3.list.tmp
    get_external_dependency_list wicked --alldeps >> external3.list.tmp
    awk ' !x[$0]++' external3.list.tmp > external3.list.tmp2 # remove duplicate lines without reordering
    awk '
        FILENAME=="external0.list" { external0[$0]++}
        FILENAME=="external1.list" { external1[$0]++}
        FILENAME=="external2.list" { external2[$0]++}
        FILENAME=="external3.list.tmp2" { if (external0[$0]==0 && external1[$0]==0 && external2[$0]==0) {print $0} }
        ' external0.list external1.list external2.list external3.list.tmp2 > external3.list # remove external0 and external1
    rm external3.list.tmp external3.list.tmp2
    ;;
  external4.list)
    echo -n > external4.list.tmp
    for p in "$horde_git"/framework/*/package.xml "$horde_git"/*/package.xml "$horde_git"/bundles/*/package.xml; do
      pear_name="$(pkgtools --sourcedirectory "$(dirname $p)" phppear name)"
      get_external_dependency_list "$pear_name" >> external4.list.tmp
    done
    awk '
        FILENAME=="external0.list" { external0[$0]++}
        FILENAME=="external1.list" { external1[$0]++}
        FILENAME=="external2.list" { external2[$0]++}
        FILENAME=="external3.list" { external3[$0]++}
        FILENAME=="external4.list.tmp" { if (external0[$0]==0 && external1[$0]==0 && external2[$0]==0 && external3[$0]==0) {print $0} }
        ' external0.list external1.list external2.list external3.list external4.list.tmp > external4.list # remove external0, external1 and external2
    rm external4.list.tmp
    ;;
  all)
    $0 ring0.list
    $0 ring1.list
    $0 ring2.list
    $0 ring3.list
    $0 ring4.list
    $0 external0.list
    $0 external1.list
    $0 external2.list
    $0 external3.list
    $0 external4.list
    ;;
  *)
    echo "Usage: $0 ring[0-4]|external[0-4]|all"
    exit 1
    ;;
esac
