#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"

owner="Mike Gabriel <sunweaver@debian.org>"
to="Debian Bug Tracking System <submit@bugs.debian.org>"
#to=$owner
headers="X-Debbugs-Cc: pkg-horde-hackers@lists.alioth.debian.org"

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping$Color_Off"
        continue
    fi
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
	if deb_exists sid "${debian_name}"; then
		echo_color "- ${Green}Already exists in sid. Skipping to next package${Color_Off}"
		continue
	fi
    if [ -f "${git_dir}/package.xml" ]; then
        dir="${git_dir}"
    else
    	ret=0
		dir=$(get_pear_package_dir $pear_name) || ret=$?
		if [ $ret != 0 ]; then
			echo_color "- ${Green}not found. Skipping to next package${Color_Off}"
			continue
		fi
    fi
	package="$(pkgtools --sourcedirectory "$dir" phppear name)"
	version="$(pkgtools --sourcedirectory "$dir" phppear version)"
	maintainers="$(pkgtools --sourcedirectory "$dir" phppear maintainers)"
	license="$(pkgtools --sourcedirectory "$dir" phppear license)"
	summary="$(pkgtools --sourcedirectory "$dir" phppear summary)"
	description="$(pkgtools --sourcedirectory "$dir" phppear description)"
	url="http://${pear_channel}/"
	if [ "${pear_channel}" = 'pear.horde.org' ]; then
		url='http://horde.org/'
	fi

	mail -s "ITP: $debian_name -- $summary" -a "$headers" "$to" <<EOF
Package: wnpp
Severity: wishlist
Owner: $owner

 Package name    : $package
 Version         : $version
 Upstream Author : $maintainers
 URL             : $url
 License         : $license
 Programming Lang: PHP
 Description     : $summary

 $description
 .
 Unfortunately, this package has recently been removed from Debian
 unstable.
 .
 I am planning to re-upload this package and pick up maintenance of Horde
 in Debian..

EOF

done 7< "$pear_name_file"
