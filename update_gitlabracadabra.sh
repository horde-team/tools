#!/bin/bash

set -e

. `dirname $0`/functions.sh

cat <<EOF > gitlabracadabra.yml
.template:
  create_object: true
  name: ''
  default_branch: debian-sid
  description: ''
  issues_access_level: disabled
  repository_access_level: enabled
  merge_requests_access_level: enabled
  builds_access_level: enabled
  wiki_access_level: disabled
  snippets_access_level: disabled
  resolve_outdated_diff_discussions: false
  build_git_strategy: fetch
  build_timeout: 3600
  auto_cancel_pending_pipelines: enabled
  build_coverage_regex: ''
  ci_config_path: 'debian/salsa-ci.yml'
  ci_default_git_depth:
  auto_devops_enabled: false
  auto_devops_deploy_strategy: continuous
  container_registry_enabled: false
  shared_runners_enabled: true
  visibility: public
  public_builds: true
  only_allow_merge_if_pipeline_succeeds: true
  only_allow_merge_if_all_discussions_are_resolved: true
  merge_method: ff
  lfs_enabled: true
  packages_enabled: true
  request_access_enabled: true
  tag_list: [Debian, Horde]
  # avatar
  printing_merge_request_link_enabled: true
  initialize_with_readme: false
  unknown_milestones: warn
  #  branches: []
  #groups: {}
  unknown_groups: warn
  members: 
    sathieu: owner
    sunweaver: owner
    gratuxri: maintainer
    roberto: developer
  unknown_members: warn
  protected_branches:
    debian-sid:
      merge_access_level: developer
      push_access_level: developer
    pristine-tar:
      merge_access_level: developer
      push_access_level: developer
    upstream-sid:
      merge_access_level: developer
      push_access_level: developer
  unknown_protected_branches: warn
  protected_tags:
    '*': developer
  unknown_protected_tags: warn
  archived: false
  variables: []
  unknown_variables: warn
  emails_disabled: false
  labels:
    - name: bug
      color: '#d9534f'
      description: ''
    - name: confirmed
      color: '#d9534f'
      description: ''
    - name: critical
      color: '#d9534f'
      description: ''
    - name: discussion
      color: '#428bca'
      description: ''
    - name: documentation
      color: '#f0ad4e'
      description: ''
    - name: enhancement
      color: '#5cb85c'
      description: ''
    - name: suggestion
      color: '#428bca'
      description: ''
    - name: support
      color: '#f0ad4e'
      description: ''
  unknown_labels: warn

EOF
for pear_name_file in ring{0,1,2,3,4}.list; do
    while read -u 7 pear_line; do
        if echo "$pear_line" | grep -q '^#'; then
                # skip comment
                continue
        fi
        pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
        pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
        if [ "${pear_name}" = '' ]; then
            pear_name="${pear_channel}"
            pear_channel='pear.horde.org'
        fi
        # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
        cd "$pkg_horde_dir/tools"
        debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
        git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
        cat <<EOF >> gitlabracadabra.yml
horde-team/$debian_name:
  extends: .template
  name: $debian_name
  description: $debian_name packaging

EOF
    done 7< "$pear_name_file"
done
