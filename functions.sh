
set -e

if [ ! -f `dirname $0`/config.sh ]; then
	echo 'Config file is missing. See README.'
	exit 1
fi

. `dirname $0`/config.sh

# ====== #
# COLORS #
# ====== #
# Reset
Color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensty
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensty
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensty backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[10;95m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White

echo_color() {
	echo -e $@
}

# ======= #
# Helpers #
# ======= #
get_pear_package_dir() {
  pear_package="$1"
  pear_package_truncated=$(echo $pear_package | sed s/^horde_//i)
  if [ -z $pear_package -o -z $pear_package_truncated ]; then
    echo "Incorrect PEAR package '$pear_package'." 1>&2
    return 1
  fi
  if [ -d "$horde_git/framework/$pear_package_truncated" ]; then
    echo -n "$horde_git/framework/$pear_package_truncated"
  elif [ -d "$horde_git/$pear_package_truncated" ]; then
    echo -n "$horde_git/$pear_package_truncated"
  elif [ -d "$horde_git/$pear_package" ]; then
    echo -n "$horde_git/$pear_package"
  elif [ -d "$horde_git/bundles/$pear_package" ]; then
    echo -n "$horde_git/bundles/$pear_package"
  else
    echo "Unable to find source directory for '$pear_package'." 1>&2
    return 1
  fi
}

get_git_dir() {
	local pear_channel="$1"
	local pear_package="$2"
	local debian_name="$3"
	debian_name=$(echo "${debian_name}" | sed 's/^php5-/php-/')
	if [ "${pear_channel}" = 'pear.horde.org' ]; then
		echo "$pkg_horde_dir/$debian_name"
	else
		echo "$pkg_php_dir/$debian_name"
	fi

}

get_git_remote_url() {
	local pear_channel="$1"
	local pear_package="$2"
	local debian_name="$3"
	debian_src_name=$(echo "${debian_name}" | sed 's/^php5-/php-/')
	if [ "${pear_channel}" = 'pear.horde.org' ]; then
		echo "$pkg_horde_git_remote/$debian_src_name.git"
	else
		echo "$pkg_php_git_remote/$debian_src_name.git"
	fi

}

get_git_remote_dir() {
	local pear_channel="$1"
	local pear_package="$2"
	local debian_name="$3"
	echo $(get_git_remote_url "${pear_channel}" "${pear_package}" "${debian_name}" | sed 's@^\([^:]\+://\)[^/]\+/@/@' )
}

get_branch() {
	local branch_name="$1"
	local alt_name="$2"
	if [ "${alt_name}" = '' -a "${branch_name}" = 'debian' ]; then
		alt_name='master'
	fi
	for try in "${branch_name}-sid" "${branch_name}" "${alt_name}"; do
		if git show-ref --verify --quiet "refs/remotes/origin/${try}" ; then
			echo "$try"
			return 0
		fi
	done
	return 1
}

deb_exists() {
	local dist="$1"
	local packages_file="Packages-$dist"
	if [ ! -f "${packages_file}" -o $(date +%s) -gt $(($(stat --format='%Y' "${packages_file}") + 60*60*24)) ]; then
		echo_color "${Yellow}Getting Packages.xz from Debian $dist$Color_Off"
		curl "http://ftp.debian.org/debian/dists/$dist/main/binary-amd64/Packages.xz" | unxz | grep '^Package:' > "${packages_file}"
	fi
    if grep -q "^Package: ${debian_name}\$" "${packages_file}" ; then
        return 0
    fi
	return 1
}

# ======== #
# Wrappers #
# ======== #
call_php() {
  LANG=C php -d include_path="$web_dir/libs:/usr/share/php:/usr/share/pear" $@
}

call_lintian() {
  lintian --color always -I --pedantic --show-overrides $@
}


# ======== #
# Wrappers #
# ======== #
do_horde_components_distribute() {
  pear_package="$1"
  dest="$2"
  echo "======================================================================="
  echo "| do_horde_components_distribute $pear_package"
  echo "| -> $dest"
  echo "======================================================================="
  rm -r "$dest" ||:
  call_php "$web_dir/components/bin/horde-components" "$pear_package" distribute --allow-remote --templatedir="$web_dir/components/data/distribute/debian/" --destination="$dest"
}

# ================== #
# Operation on lists #
# ================== #

check_list() {
  list="$1"
  if [ -z "$list" ]; then
    echo "Missing argument, try 'ring0.list'"
    exit 1
  fi
  if [ ! -f "$1" ]; then
    echo "Non existing list '$list'."
    exit 1
  fi
}

# ======================== #
# Buildig dependency lists #
# ======================== #

get_dependency_list() {
  pear_package="$1"
  options="$2"
  pear_package_dir=$(get_pear_package_dir $pear_package) || exit $?
  call_php "$web_dir/components/bin/horde-components" --nocolor --short $options \
    "$pear_package_dir" deps | grep -F '[pear.horde.org]' | sed 's/[- ].*$//'
}

get_external_dependency_list() {
  pear_package="$1"
  options="$2"
  pear_package_dir=$(get_pear_package_dir $pear_package) || exit $?
  call_php "$web_dir/components/bin/horde-components" --nocolor --short $options \
    "$pear_package_dir" deps | grep -vF '[pear.horde.org]' \
    | grep -vF 'The list contains optional dependencies!' \
    | grep -vF 'Dependencies on PEAR itself are not displayed.' \
    | grep -vF 'The list only contains required dependencies!' \
    | grep -v '^$' \
    | sed 's@\[PHP Extension\]@[extension]@' \
    | sed 's/^\([a-z0-9_-]\+\)\s\+\[\([a-z0-9.]\+\)\]\s*$/\2 \1/i'
}

