#!/bin/bash

set -e

. `dirname $0`/functions.sh

pear_name_file="$1"
check_list "$pear_name_file"
failed_packages_count=0
failed_packages=""

while read -u 7 pear_line; do
	if echo "$pear_line" | grep -q '^#'; then
		# skip comment
		continue
	fi
    pear_name=$(echo "${pear_line}" | cut -s -d\  -f2)
    pear_channel=$(echo "${pear_line}" | cut -d\  -f1)
    if [ "${pear_name}" = '' ]; then
		pear_name="${pear_channel}"
		pear_channel='pear.horde.org'
    fi
    # Go to some trusted directory (without debian/pkg-php-tools-overrides file)
    cd "$pkg_horde_dir/tools"
    debian_name="$(pkgtools phppear debianname $pear_channel $pear_name)"
    if [ "${debian_name}" = '' ]; then
		echo_color "${Green}$pear_channel/$pear_name: skipping$Color_Off"
        continue
    fi
	git_dir=$(get_git_dir "${pear_channel}" "${pear_name}" "${debian_name}")
	echo_color "$Green$debian_name ($pear_channel/$pear_name)$Color_Off"
    echo_color "$Green============------------$Color_Off"
    if [ ! -d "${git_dir}/.git" ]; then
        echo_color "- ${Yellow}Git dir not present. Skipping to next package${Color_Off}"
        continue
    fi
    cd $git_dir
	debian_branch=$(get_branch 'debian')
    git checkout "${debian_branch}"
    ret=0
    gbp buildpackage || ret=$?
    if [ $ret -ne 0 ]; then
        failed_packages_count=$((failed_packages_count+1))
        failed_packages="$failed_packages $debian_name"
    fi
done 7< "$pear_name_file"
if [ $failed_packages_count -gt 0 ]; then
    echo_color "$Red$failed_packages_count packages have failed to build$Color_Off: $failed_packages"
fi
